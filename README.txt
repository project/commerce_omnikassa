CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
Commerce Omnikassa

This module provides payment provider for Rabo Omnikassa.
This is a payment platform provided by the Rabobank in the Netherlands.
It uses v2 of the api, provided by the opensdks/omnikassa2-sdk library.
(https://github.com/opensdks/omnikassa2-sdk)

Issues
https://www.drupal.org/project/issues/commerce_omnikassa

REQUIREMENTS
------------
Commerce

INSTALLATION
------------
- Install with composer 
composer require 'drupal/commerce_omnikassa'
- Enable the module

CONFIGURATION
-------------
* Add a payment method (/admin/commerce/config/payment-gateways/add)
* Select the Omnikassa-plugin
* Provide the token and signing key, and select wether you are on test or production
* Copy the IPN-URL on the edit-page once you added the payment method
* Setup this URL in your dashboard for Rabo Omnikassa at the Rabobank as Webhook.

MAINTAINERS
-----------
Current maintainers:
 * Josha Hubbers (JoshaHubbers) - https://www.drupal.org/u/joshahubbers-0

