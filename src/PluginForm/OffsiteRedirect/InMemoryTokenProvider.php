<?php

namespace Drupal\commerce_omnikassa\PluginForm\OffsiteRedirect;

use nl\rabobank\gict\payments_savings\omnikassa_sdk\connector\TokenProvider;

/**
 * Handles the RaboOmnikassa tokens.
 */
class InMemoryTokenProvider extends TokenProvider {

  /**
   * Stores token values.
   *
   * @var array
   */
  private $map = [];

  /**
   * Construct the in memory token provider with the given refresh token.
   *
   * @param string $refreshToken
   *   The refresh token used to retrieve the access tokens with.
   */
  public function __construct($refreshToken) {
    $this->setValue(static::REFRESH_TOKEN, $refreshToken);
  }

  /**
   * Retrieve the value for the given key.
   *
   * @param string $key
   *   The key.
   *
   * @return string
   *   Value of the given key or null if it does not exists.
   */
  protected function getValue($key) {
    return array_key_exists($key, $this->map) ? $this->map[$key] : NULL;
  }

  /**
   * Store the value by the given key.
   *
   * @param string $key
   *   The key.
   * @param string $value
   *   The value.
   */
  protected function setValue($key, $value) {
    $this->map[$key] = $value;
  }

  /**
   * Optional functionality to flush your systems.
   *
   * It is called after storing all the values of the access token
   * and can be used for example to clean caches or reload changes from the
   * database.
   */
  protected function flush() {
  }

}
