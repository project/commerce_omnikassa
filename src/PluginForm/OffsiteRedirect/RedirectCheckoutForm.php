<?php

namespace Drupal\commerce_omnikassa\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\Money;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\OrderItem;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\PaymentBrandForce;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\ProductType;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\request\MerchantOrder;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\Address;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\CustomerInformation;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\Environment;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\signing\SigningKey;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\endpoint\Endpoint;

/**
 * {@inheritDoc}
 */
class RedirectCheckoutForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getEntity();
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();
    foreach ($configuration['omnikassa_payment_brands'] as $key => $value) {
      if ($key == $value) {
        $omnikassa_available_options[$key] = $value;
      }
    }
    if (count($omnikassa_available_options) == 1 ||
      array_key_exists('OMNIKASSA', $omnikassa_available_options)) {
      if (array_key_exists('OMNIKASSA', $omnikassa_available_options)) {
        $payment_method = 'OMNIKASSA';
      }
      else {
        $payment_method = key($omnikassa_available_options);
      }
      // Only one payment method, redirect to Rabo Omnikassa.
      return $this->submitOmnikassaPayment($form, $form_state, $payment, $payment_method);
    }
    else {
      // Multiple payment methods available, present select form.
      $form['omnikassa_select_payment'] = [
        '#type' => 'select',
        '#title' => $this->t('Payment method'),
        '#description' => $this->t('Select which payment method you want to use.'),
        '#default_value' => '',
        '#options' => $omnikassa_available_options,
        '#required' => TRUE,
      ];
      $form['actions'] = ['#type' => 'actions'];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Complete payment'),
      ];
      $form['actions']['cancel'] = [
        '#type' => 'link',
        '#title' => $this->t('Cancel'),
        '#url' => Url::fromUri($form['#cancel_url']),
      ];

      return $form;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getEntity();
    return $this->submitOmnikassaPayment($form, $form_state, $payment, $values['omnikassa_select_payment']);
  }

  /**
   * Function redirects to the payment gateway.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   * @param string $payment_method
   *   The selected payment method.
   *
   * @return array
   *   Response of the redirect form.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  private function submitOmnikassaPayment(array $form,
                                          FormStateInterface $form_state,
                                          PaymentInterface $payment,
                                          string $payment_method) {

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    $order = $this->buildOmnikassaOrder($form, $payment, $payment_method);

    if ($configuration['mode'] == 'test') {
      $omnikassa_environment = Environment::SANDBOX;
    }
    else {
      $omnikassa_environment = Environment::PRODUCTION;
    }
    $signingKey = new SigningKey(base64_decode($configuration['omnikassa_signing_key']));
    $inMemoryTokenProvider = new InMemoryTokenProvider($configuration['omnikassa_token']);
    $endpoint = Endpoint::createInstance($omnikassa_environment, $signingKey, $inMemoryTokenProvider);
    $redirectUrl = $endpoint->announceMerchantOrder($order);

    $data = [];

    // Save the payment in case we don't return properly.
    $payment->save();

    return $this->buildRedirectForm(
      $form,
      $form_state,
      $redirectUrl,
      $data,
      self::REDIRECT_GET
    );
  }

  /**
   * Function creates a valid Rabo Omnikassa order.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   Payment provider configuration.
   * @param string $payment_method
   *   The selected payment method.
   *
   * @return \nl\rabobank\gict\payments_savings\omnikassa_sdk\model\request\MerchantOrder
   *   Valid order.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  private function buildOmnikassaOrder(array $form,
                                       PaymentInterface $payment,
                                       string $payment_method) {

    $order = $payment->getOrder();

    foreach ($order->getItems() as $order_item) {

      $product_variation = $order_item->getPurchasedEntity();

      $order_items[] = OrderItem::createFrom([
        'id' => $product_variation->id(),
        'name' => $product_variation->getTitle(),
        'description' => $product_variation->getTitle(),
        'quantity' => $order_item->getQuantity(),
        'amount' => Money::fromDecimal('EUR', $product_variation->getPrice()->getNumber()),
        'category' => ProductType::DIGITAL,
      ]);

      /* TODO:
      - category can be DIGITAL or PHYSICAL
      - Should we add VAT?
      'tax' => Money::fromDecimal('EUR', 00.00),
      'vatCategory'
      1 = Hoog (momenteel 21%),
      2 = Laag (vanaf 1 januari 2019 is dit 9%),
      3 = Nul (0%),
      4 = Geen (vrijgesteld van BTW)
       */

    }

    // Get addresses (Shipping and Billing).
    $profiles = $order->collectProfiles();
    if (isset($profiles['billing'])) {
      $billing_profile = $profiles['billing'];
      $billing_address = $billing_profile->get('address')->first();
      $billing_details = Address::createFrom([
        'firstName' => $billing_address->getGivenName(),
        'middleName' => '',
        'lastName' => $billing_address->getFamilyName(),
        'street' => $billing_address->getAddressLine1() . ' ' . $billing_address->getAddressLine2(),
        'postalCode' => $billing_address->getPostalCode(),
        'city' => $billing_address->getLocality(),
        'countryCode' => $billing_address->getCountryCode(),
        'houseNumber' => '',
        'houseNumberAddition' => '',
      ]);
    }
    if (isset($profiles['shipping'])) {
      $shipping_profile = $profiles['shipping'];
      $shipping_address = $shipping_profile->get('address')->first();
      $shipping_details = Address::createFrom([
        'firstName' => $shipping_address->getGivenName(),
        'middleName' => '',
        'lastName' => $shipping_address->getFamilyName(),
        'street' => $shipping_address->getAddressLine1() . ' ' . $shipping_address->getAddressLine2(),
        'postalCode' => $shipping_address->getPostalCode(),
        'city' => $shipping_address->getLocality(),
        'countryCode' => $shipping_address->getCountryCode(),
        'houseNumber' => '',
        'houseNumberAddition' => '',
      ]);
    }
    else {
      // If billing address isset, and shipping is not, they are the same.
      if (isset($billing_details)) {
        $shipping_details = $billing_details;
      }
    }

    $customerInformation = CustomerInformation::createFrom([
      'emailAddress' => $order->getEmail(),
    ]);

    $site_config = \Drupal::config('system.site');
    $data = [
      'merchantOrderId' => $payment->getOrderId(),
      'description' => $site_config->get('name') . ' order ' . $payment->getOrderId(),
      'amount' => Money::fromDecimal('EUR', $payment->getAmount()->getNumber()),
      'customerInformation' => $customerInformation,
      'language' => 'NL',
      'merchantReturnURL' => $form['#return_url'],
    ];
    if (isset($shipping_details)) {
      $data['shippingDetail'] = $shipping_details;
    }
    if (isset($billing_details)) {
      $data['billingDetail'] = $billing_details;
    }
    if (isset($order_items)) {
      $data['orderItems'] = $order_items;
    }
    if ($payment_method != 'OMNIKASSA') {
      $data['paymentBrand'] = $payment_method;
      $data['paymentBrandForce'] = PaymentBrandForce::FORCE_ONCE;
    }

    return MerchantOrder::createFrom($data);
  }

}
