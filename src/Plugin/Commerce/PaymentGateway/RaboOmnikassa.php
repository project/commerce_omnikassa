<?php

namespace Drupal\commerce_omnikassa\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_omnikassa\PluginForm\OffsiteRedirect\InMemoryTokenProvider;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\endpoint\Endpoint;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\Environment;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\response\AnnouncementResponse;
use ReflectionClass;
use Symfony\Component\HttpFoundation\Request;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\signing\SigningKey;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\response\PaymentCompletedResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Rabo Omnikassa payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "omnikassa_redirect_checkout",
 *   label = @Translation("Omnikassa (Redirect to the Rabo Omnikassa)"),
 *   display_label = @Translation("Rabo Omnikassa"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_omnikassa\PluginForm\OffsiteRedirect\RedirectCheckoutForm",
 *   },
 * )
 */
class RaboOmnikassa extends OffsitePaymentGatewayBase {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new PaymentGatewayBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, LoggerChannelFactoryInterface $logger_channel_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->logger = $logger_channel_factory->get('commerce_omnikassa');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'omnikassa_token' => '',
      'omnikassa_signing_key' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['omnikassa_token'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Token'),
      '#description' => $this->t('Enter the token from your Dashboard.'),
      '#default_value' => $this->configuration['omnikassa_token'],
      '#required' => TRUE,
    ];

    $form['omnikassa_signing_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Signing key'),
      '#description' => $this->t('The signing key from your Dashboard.'),
      '#default_value' => $this->configuration['omnikassa_signing_key'],
      '#required' => TRUE,
    ];

    if (!empty($form_state->getValue('id'))) {
      $form['omnikassa_ipn_url'] = ['#markup' => $this->t('<div>
<strong>Webhook:</strong><br/>Rabo omnikassa can send you notifications if the
status of a payment changes. Then the payment-status in the site is updated
to match the status at the provider.<br/>The url for the webhook is:<br/>
<em>@webhook_url</em><br/>
You can configure this in the Rabo omnikassa-control panel.</div>',[
        '@webhook_url' => Url::fromRoute('commerce_payment.notify',
          ['commerce_payment_gateway' => $form_state->getValue('id')])->setAbsolute(TRUE)->toString()]),
        '#allowed_tags' => ['div',], ];
    }

    $omnikassa_paymentbrand_class = new ReflectionClass('nl\rabobank\gict\payments_savings\omnikassa_sdk\model\PaymentBrand');
    $omnikassa_payment_brands = $omnikassa_paymentbrand_class->getConstants();
    $omnikassa_payment_brands['OMNIKASSA'] = 'EXTERNAL';
    $form['omnikassa_payment_brands'] = [
      '#type' => 'checkboxes',
      '#options' => $omnikassa_payment_brands,
      '#title' => $this->t('What paymenttypes do you accept?'),
      '#description' => $this->t('Select the payment options that can be used.
      <ul><li>The option \'CARDS\' will enable all card-types:<br/>
      MASTERCARD, VISA, BANCONTACT, MAESTRO and V_PAY.<br/>
      The right card is chosen at the Payment provider.</li>
      <li>The option \'EXTERNAL\' will skip all options above.<br/>
      The payment option will be chosen at the Payment provider.</li></ul>'),
      '#default_value' => $this->configuration['omnikassa_payment_brands'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['omnikassa_token'] = $values['omnikassa_token'];
    $this->configuration['omnikassa_signing_key'] = $values['omnikassa_signing_key'];
    $this->configuration['omnikassa_payment_brands'] = $values['omnikassa_payment_brands'];
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    // Validate response.
    $orderId = $request->get('order_id');
    $status = $request->get('status');
    $signature = $request->get('signature');
    $signingKey = new SigningKey(base64_decode($this->configuration['omnikassa_signing_key']));
    // Validate and sanitize response with PaymentCompletedResponse.
    $paymentCompletedResponse = PaymentCompletedResponse::createInstance($orderId, $status, $signature, $signingKey);
    if (!$paymentCompletedResponse) {
      $this->logger->error('Payment response validation failed.
      Orderid: @order_id.
      Status: @status.
      Signature: @signature',
        [
          '@order_id' => $orderId,
          '@status' => $status,
          '@signature' => $signature,
        ]
      );
    }
    else {
      // Check if order was completed.
      if ($paymentCompletedResponse->getStatus() != 'COMPLETED') {
        $this->messenger()->addMessage($this->t('You have canceled checkout at Rabo Omnikassa.
        Please contact us how to complete your order.'),
          MessengerInterface::TYPE_WARNING);
      }
      else {
        $this->messenger()->addMessage($this->t('Your payment was successful!'));
      }
      // First check if the payment was saved before
      try {
        $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
        $payments = $payment_storage->loadByProperties(['order_id' => $order->id()]);
        // Update the existing payment.
        if (count($payments) > 0) {
          foreach ($payments as $payment) {
            $payment->setState($this->getStatusMapping($paymentCompletedResponse->getStatus()));
            $payment->setRemoteId($paymentCompletedResponse->getOrderID());
            $payment->setRemoteState($paymentCompletedResponse->getStatus());
            try {
              $payment->save();
            } catch (EntityStorageException $e) {
              $this->logger->error('Failed saving payment in onReturn update payment.');
            }
          }
        }
        else {
          // No payment yet, save a new one.
          $payment = $payment_storage->create([
            'state' => $this->getStatusMapping($paymentCompletedResponse->getStatus()),
            'amount' => $order->getBalance(),
            'payment_gateway' => $this->parentEntity->id(),
            'order_id' => $order->id(),
            'remote_id' => $paymentCompletedResponse->getOrderID(),
            'remote_state' => $paymentCompletedResponse->getStatus(),
          ]);
          try {
            $payment->save();
          } catch (EntityStorageException $e) {
            $this->logger->error('Failed saving payment in onReturn create payment.');
          }
        }
      } catch (InvalidPluginDefinitionException $e) {
      } catch (PluginNotFoundException $e) {
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $omnikassa_key = $this->configuration['omnikassa_signing_key'];
    $omnikassa_token = $this->configuration['omnikassa_token'];
    $omnikassa_env = $this->configuration['mode'];

    if ($omnikassa_env == 'test') {
      $omnikassa_environment = Environment::SANDBOX;
    }
    else {
      $omnikassa_environment = Environment::PRODUCTION;
    }
    $signingKey = new SigningKey(base64_decode($omnikassa_key));
    $inMemoryTokenProvider = new InMemoryTokenProvider($omnikassa_token);
    $endpoint = Endpoint::createInstance($omnikassa_environment, $signingKey, $inMemoryTokenProvider);

    $json = $request->getContent();

    if ($json != '' && strpos($json, 'authentication') !== FALSE) {
      $announcementResponse = new AnnouncementResponse($json, $signingKey);
      do {
        $response = $endpoint->retrieveAnnouncement($announcementResponse);
        $results = $response->getOrderResults();
        foreach ($results as $result) {
          $payment_order_id = $result->getMerchantOrderId();

          try {
            $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
            $payments = $payment_storage->loadByProperties(['order_id' => $payment_order_id]);
            // Update the existing payment.
            if (count($payments) > 0) {
              foreach ($payments as $payment) {
                $payment->setState($this->getStatusMapping($result->getOrderStatus()));
                $payment->setRemoteId($result->getOmnikassaOrderId());
                $payment->setRemoteState($result->getOrderStatus());
                try {
                  $payment->save();
                  $this->logger->info('Omnikassa IPN-call received: order-> @order_id, status-> @status',
                    [
                      '@order_id' => $payment_order_id,
                      '@status' => $result->getOrderStatus(),
                    ]
                  );
                } catch (EntityStorageException $e) {
                  $this->logger->error('Omnikassa IPN-call received, payment-update failed: order-> @order_id, status-> @status',
                    [
                      '@order_id' => $payment_order_id,
                      '@status' => $result->getOrderStatus(),
                    ]
                  );
                }
              }
            }
            else {
              // No payment yet, save a new one.
              $payment = $payment_storage->create([
                'state' => $this->getStatusMapping($result->getOrderStatus()),
                'amount' => $result->getPaidAmount(),
                'payment_gateway' => 'omnikassa',
                'order_id' => $payment_order_id,
                'remote_id' => $result->getOmnikassaOrderId(),
                'remote_state' => $result->getOrderStatus(),
              ]);
              try {
                $payment->save();
                $this->logger->info('Omnikassa IPN-call received: order-> @order_id, status-> @status',
                  [
                    '@order_id' => $payment_order_id,
                    '@status' => $result->getOrderStatus(),
                  ]
                );
              } catch (EntityStorageException $e) {
                $this->logger->error('Omnikassa IPN-call received, payment-update failed: order-> @order_id, status-> @status',
                  [
                    '@order_id' => $payment_order_id,
                    '@status' => $result->getOrderStatus(),
                  ]
                );
              }
            }
          } catch (InvalidPluginDefinitionException $e) {
          } catch (PluginNotFoundException $e) {
          }
        }
      }
      while ($response->isMoreOrderResultsAvailable());
    }
  }

  /**
   * Returns a mapping of Omnikassa payment statuses to payment states.
   *
   * @param string $status
   *   (optional) The Omnikassa payment status.
   *
   * @return array|string
   *   An array containing the Omnikassa remote statuses as well as their
   *   corresponding states. if $status is specified, the corresponding state
   *   is returned.
   */
  protected function getStatusMapping($status = NULL) {
    $mapping = [
      'CANCELLED' => 'authorization_voided',
      'COMPLETED' => 'completed',
      'EXPIRED' => 'authorization_expired',
    ];

    // If a status was passed, return its corresponding payment state.
    if (isset($status) && isset($mapping[$status])) {
      return $mapping[$status];
    }

    return $mapping;
  }

}
